package com.edugaon.googleauth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger

class DashboardActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var callbackManager: CallbackManager
    private lateinit var buttonFacebookLogin: LoginButton
    private lateinit var fb:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        auth = FirebaseAuth.getInstance()
        fb = findViewById(R.id.fb)
        buttonFacebookLogin = findViewById(R.id.facebookLogin_button)
        callbackManager = CallbackManager.Factory.create()

        buttonFacebookLogin.setReadPermissions("email", "public_profile", "user_friends");
        buttonFacebookLogin.setOnClickListener {
            faceBookSignIn()
        }

    }
     fun onClick(v: View) {
        if (v === fb) {
            buttonFacebookLogin.performClick()
        }
    }

    private fun faceBookSignIn() {
        buttonFacebookLogin.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    handleFacebookAccessToken(loginResult.accessToken)
                }

                override fun onCancel() {
                }

                override fun onError(error: FacebookException) {
                }
            })
    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    if (user != null) {
                        updateUI(user)
                    }
                    Toast.makeText(this, "You are Loging on Facebook", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this,
                        "Authentication failed." + task.exception,
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        val userFacebookName = findViewById<TextView>(R.id.facebookName_textView)
        val userFacebookEmail = findViewById<TextView>(R.id.facebookEmail_textView)
        val userFacebookPhone = findViewById<TextView>(R.id.facebookPhone_textView)
        userFacebookName.setText(user?.displayName)
        userFacebookEmail.setText(user?.email)
        userFacebookPhone.setText(user?.phoneNumber)

//        val intent = Intent(this, MainActivity::class.java)
//        startActivity(intent)
//        finish()
    }
}
